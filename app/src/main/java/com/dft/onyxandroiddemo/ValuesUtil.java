package com.dft.onyxandroiddemo;

import android.app.Activity;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.dft.onyxcamera.config.Onyx;
import com.dft.onyxcamera.config.OnyxConfiguration;
import com.dft.onyxcamera.ui.reticles.Reticle;

/**
 * Created by BigWheat on 2/10/2018.
 */

public class ValuesUtil {

    public static boolean getReturnRawImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnRawImage)).isChecked();
    }

    public static boolean getReturnProcessedImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnProcessedImage)).isChecked();
    }
    public static boolean getShouldBinarizeProcessedImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setShouldBinarizeProcessedImage)).isChecked();
    }
    public static boolean getReturnEnhancedImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnEnhancedImage)).isChecked();
    }

    public static boolean getReturnSlapImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnSlapImage)).isChecked();
    }

    public static boolean getReturnSlapWsqData(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnSlapWsqData)).isChecked();
    }

    public static boolean getReturnFullFrameImage(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnFullFrameImage)).isChecked();
    }

    public static float getFullFrameMaxImageHeight(Activity a) {
        EditText text = ((EditText) a.findViewById(R.id.fullFrameMaxImageHeight));
        return Float.parseFloat(text.getText().toString());
    }

    public static boolean getReturnWSQ(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnWSQ)).isChecked();
    }

    public static boolean getUseOnyxLive(Activity a) {
        return ((Switch) a.findViewById(R.id.setUseOnyxLive)).isChecked();
    }

    public static boolean getComputeNfiqMetrics(Activity a) {
        return ((Switch) a.findViewById(R.id.setComputeNfiqMetrics)).isChecked();
    }

    public static float getCropFactor(Activity a) {
        EditText cropFactor = ((EditText) a.findViewById(R.id.OnyxConfig_CropFactor));
        return Float.parseFloat(cropFactor.getText().toString());
    }

    public static float getCropSizeWidth(Activity a) {
        EditText sizeWidth = ((EditText) a.findViewById(R.id.OnyxConfig_CropSize_Width));
        return Float.parseFloat(sizeWidth.getText().toString());
    }

    public static float getCropSizeHeight(Activity a) {
        EditText sizeHeight = ((EditText) a.findViewById(R.id.OnyxConfig_CropSize_Height));
        return Float.parseFloat(sizeHeight.getText().toString());
    }

    public static boolean getShowLoadingSpinner(Activity a) {
        return ((Switch) a.findViewById(R.id.showLoadingSpinner)).isChecked();
    }

    public static boolean getUseManualCapture(Activity a) {
        return ((Switch) a.findViewById(R.id.useManualCapture)).isChecked();
    }

    public static String getManualCaptureText(Activity a) {
        EditText editText = ((EditText) a.findViewById(R.id.manualCaptureText));
        return editText.getText().toString();
    }

    public static String getCaptureFingersText(Activity a) {
        EditText editText = ((EditText) a.findViewById(R.id.captureFingersText));
        return editText.getText().toString();
    }

    public static String getCaptureThumbText(Activity a) {
        EditText editText = ((EditText) a.findViewById(R.id.captureThumbText));
        return editText.getText().toString();
    }

    public static String getFingersNotInFocusText(Activity a) {
        EditText editText = ((EditText) a.findViewById(R.id.fingersNotInFocusText));
        return editText.getText().toString();
    }

    public static String getThumbNotInFocusText(Activity a) {
        EditText editText = ((EditText) a.findViewById(R.id.thumbNotInFocusText));
        return editText.getText().toString();
    }

    public static boolean getUseFlash(Activity a) {
        return ((Switch) a.findViewById(R.id.setUseFlash)).isChecked();
    }

    public static boolean getReturnOnyxErrorOnLowQuality(Activity a) {
        return ((Switch) a.findViewById(R.id.setReturnOnyxErrorOnLowQuality)).isChecked();
    }

    public static boolean getUploadMetrics(Activity a) {
        return ((Switch) a.findViewById(R.id.uploadMetrics)).isChecked();
    }

    public static float getCaptureQualityThreshold(Activity a) {
        EditText sizeHeight = ((EditText) a.findViewById(R.id.captureQualityThreshold));
        return Float.parseFloat(sizeHeight.getText().toString());
    }

    public static int getFingerDetectionTimeout(Activity a) {
        EditText sizeHeight = ((EditText) a.findViewById(R.id.fingerDetectionTimeout));
        return Integer.parseInt(sizeHeight.getText().toString());
    }

    public static boolean getTriggerCaptureOnFingerDetectionTimeout(Activity a) {
        return ((Switch) a.findViewById(R.id.triggerCaptureOnFingerDetectionTimeout)).isChecked();
    }

    public static OnyxConfiguration.FingerprintTemplateType getReturnFingerprintTemplate(Activity a) {
        String fingerprintTemplateType = ((Spinner) a.findViewById(R.id.returnFingerprintTemplateSpinner))
                .getSelectedItem().toString();
        OnyxConfiguration.FingerprintTemplateType templateType = OnyxConfiguration.FingerprintTemplateType.NONE;
        if (fingerprintTemplateType.equalsIgnoreCase(OnyxConfiguration.FingerprintTemplateType.INNOVATRICS.toString())) {
            templateType = OnyxConfiguration.FingerprintTemplateType.INNOVATRICS;
        } else if (fingerprintTemplateType.equalsIgnoreCase(OnyxConfiguration.FingerprintTemplateType.ISO.toString())) {
            templateType = OnyxConfiguration.FingerprintTemplateType.ISO;
        }
        return templateType;
    }

    public static Reticle.Orientation getReticleOrientation(Activity a) {
        String reticleOrientation = ((Spinner) a.findViewById(R.id.reticleOrientationSpinner)).getSelectedItem().toString();
        Reticle.Orientation orientation = Reticle.Orientation.LEFT;
        if (reticleOrientation.equalsIgnoreCase(Reticle.Orientation.LEFT.toString())) {
            orientation = Reticle.Orientation.LEFT;
        } else if (reticleOrientation.equalsIgnoreCase(Reticle.Orientation.RIGHT.toString())) {
            orientation = Reticle.Orientation.RIGHT;
        } else if (reticleOrientation.equalsIgnoreCase(Reticle.Orientation.THUMB_PORTRAIT.toString())) {
            orientation = Reticle.Orientation.THUMB_PORTRAIT;
        }
        return orientation;
    }


}
