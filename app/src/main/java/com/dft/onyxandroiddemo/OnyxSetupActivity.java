package com.dft.onyxandroiddemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.dft.onyxcamera.config.Onyx;
import com.dft.onyxcamera.config.OnyxConfiguration;
import com.dft.onyxcamera.config.OnyxConfigurationBuilder;
import com.dft.onyxcamera.config.OnyxError;
import com.dft.onyxcamera.config.OnyxResult;

import timber.log.Timber;

import static com.dft.onyxandroiddemo.ValuesUtil.*;

public class OnyxSetupActivity extends Activity {
    private static final String TAG = OnyxSetupActivity.class.getName();
    private static final int ONYX_REQUEST_CODE = 1337;
    MainApplication application = new MainApplication();
    private Activity activity;
    private AlertDialog alertDialog;

    private OnyxConfiguration.SuccessCallback successCallback;
    private OnyxConfiguration.ErrorCallback errorCallback;
    private OnyxConfiguration.OnyxCallback onyxCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getActionBar().hide();
        } catch (NullPointerException e){}

        new FileUtil().getWriteExternalStoragePermission(this); // This is for file writing permission on SDK >= 23
        setupUI();
        setupCallbacks();
    }

    private void setupCallbacks() {
        successCallback = new OnyxConfiguration.SuccessCallback() {
            @Override
            public void onSuccess(OnyxResult onyxResult) {
                application.setOnyxResult(onyxResult);
                finishActivityForRunningOnyx();
            }
        };

        errorCallback = new OnyxConfiguration.ErrorCallback() {
            @Override
            public void onError(OnyxError onyxError) {
                Log.e("OnyxError", onyxError.getErrorMessage());
                application.setOnyxError(onyxError);
                showAlertDialog(onyxError);
                finishActivityForRunningOnyx();
            }
        };

        onyxCallback = new OnyxConfiguration.OnyxCallback() {
            @Override
            public void onConfigured(Onyx configuredOnyx) {
                application.setConfiguredOnyx(configuredOnyx);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivityForResult(new Intent(activity, OnyxActivity.class), ONYX_REQUEST_CODE);
                    }
                });
            }
        };
    }

    private void finishActivityForRunningOnyx() {
        if (MainApplication.getActivityForRunningOnyx() != null) {
            MainApplication.getActivityForRunningOnyx().finish();
        }
    }

    private void setupOnyx(final Activity activity) {
        // Create an OnyxConfigurationBuilder and configure it with desired options
        OnyxConfigurationBuilder onyxConfigurationBuilder = new OnyxConfigurationBuilder()
                .configActivity(activity)
                .licenseKey(getResources().getString(R.string.onyx_license))
                .returnRawImage(getReturnRawImage(this))
                .returnProcessedImage(getReturnProcessedImage(this))
                .returnEnhancedImage(getReturnEnhancedImage(this))
                .returnSlapImage(getReturnSlapImage(this))
                .returnSlapWsqData(getReturnSlapWsqData(this))
                .shouldBinarizeProcessedImage(getShouldBinarizeProcessedImage(this))
                .returnFullFrameImage(getReturnFullFrameImage(this))
                .fullFrameMaxImageHeight(getFullFrameMaxImageHeight(this))
                .returnWSQ(getReturnWSQ(this))
                .returnFingerprintTemplate(getReturnFingerprintTemplate(this))
                .useOnyxLive(getUseOnyxLive(this))
                .reticleOrientation(getReticleOrientation(this))
                .computeNfiqMetrics(getComputeNfiqMetrics(this))
                .cropFactor(getCropFactor(this))
                .cropSize(getCropSizeWidth(this), getCropSizeHeight(this))
                .showLoadingSpinner(getShowLoadingSpinner(this))
                .useManualCapture((getUseManualCapture(this)))
                .manualCaptureText(getManualCaptureText(this))
                .captureFingersText(getCaptureFingersText(this))
                .captureThumbText(getCaptureThumbText(this))
                .fingersNotInFocusText(getFingersNotInFocusText(this))
                .thumbNotInFocusText(getThumbNotInFocusText(this))
                .useOnyxLive(getUseOnyxLive(this))
                .useFlash(getUseFlash(this))
                .reticleOrientation(getReticleOrientation(this))
                .computeNfiqMetrics(getComputeNfiqMetrics(this))
                .targetPixelsPerInch(500.0)
                .subjectId("OnyxAndroidDemoUser")
                .uploadMetrics(getUploadMetrics(this))
                .returnOnyxErrorOnLowQuality(getReturnOnyxErrorOnLowQuality(this))
                .captureQualityThreshold(getCaptureQualityThreshold(this))
                .fingerDetectionTimeout(getFingerDetectionTimeout(this))
                .triggerCaptureOnFingerDetectionTimeout(getTriggerCaptureOnFingerDetectionTimeout(this))
                .successCallback(successCallback)
                .errorCallback(errorCallback)
                .onyxCallback(onyxCallback);

        // Finally, build the OnyxConfiguration
        onyxConfigurationBuilder.buildOnyxConfiguration();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MainApplication.getOnyxResult() != null) {
            displayResults(MainApplication.getOnyxResult());
        }
    }

    private void displayResults(OnyxResult onyxResult) {
        startActivity(new Intent(this, OnyxImageryActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Timber.v("Permission: " + permissions[0] + " was " + grantResults[0]);
        }
    }

    private void setupUI() {
        activity = this;
        setContentView(R.layout.activity_main);
        Button captureButton = findViewById(R.id.captureButton);
        captureButton.bringToFront();
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.setOnyxResult(null);
                setupOnyx(activity);
            }
        });
    }

    /**
     * This displays an AlertDialog upon receiving an OnyxError, please handle appropriately for
     * your application
     *
     * @param onyxError
     */
    private void showAlertDialog(OnyxError onyxError) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle("Onyx Error");
        alertDialogBuilder.setMessage(onyxError.getErrorMessage());
        alertDialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                alertDialog.dismiss();
            }
        });

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }
}
