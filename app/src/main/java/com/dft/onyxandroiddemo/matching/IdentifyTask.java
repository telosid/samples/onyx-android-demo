package com.dft.onyxandroiddemo.matching;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dft.onyx.FingerprintTemplate;
import com.dft.onyx.FingerprintTemplateVector;
import com.dft.onyx.MatchResult;
import com.dft.onyx.core;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class IdentifyTask extends AsyncTask<IdentifyPayload, Void, MatchResult> {
    private Exception mException = null;
    private Context mContext = null;

    public IdentifyTask(Context context) {
        mContext = context;
    }

    @Override
    protected MatchResult doInBackground(IdentifyPayload... payloads) {
        MatchResult matchResult = null;
        try {
            // Create an OpenCV Mat class for the probe (the image that you want to match
            // against known template(s)
            Mat probe = new Mat();
            Utils.bitmapToMat(payloads[0].getProbe(), probe);

            // Converts the image to GRAY
            Imgproc.cvtColor(probe, probe, Imgproc.COLOR_RGB2GRAY);
            // Obtain your array of FingerprintTemplate objects, these are the known fingerprint templates
            FingerprintTemplate[] templatesToIdentifyAgainst = payloads[0].getReferenceTemplates();
            // Create a FingerprintTemplateVector from the collection
            FingerprintTemplateVector ftv = new FingerprintTemplateVector();
            // Add each of the known FingerprintTemplates to the FingerprintTemplateVector
            for (FingerprintTemplate ft : templatesToIdentifyAgainst) {
                ftv.push_back(ft);
            }
            matchResult = core.pyramidIdentify(ftv, probe);
        } catch (Exception e) {
            mException = e;
        }
        return matchResult;
    }

    @Override
    protected void onPostExecute(MatchResult matchResult) {
        if (mException != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(mException.getMessage())
                    .setCancelable(false)
                    .setNegativeButton("OK", (dialog, which) -> dialog.cancel())
                    .setTitle("Identification error");

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(mContext, createMatchString(matchResult.getScore(), matchResult.getIndex()), Toast.LENGTH_SHORT).show();
        }
    }

    private String createMatchString(double score, int index) {
        String matchString;
        if (score < 0.1) {
            matchString = "Failed";
        } else {
            matchString = "Match";
        }
        matchString += " (Score = " + String.format("%.2f", score) + ", for index "+ index +")";

        return matchString;
    }

}
