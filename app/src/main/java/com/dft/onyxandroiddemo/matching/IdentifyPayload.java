package com.dft.onyxandroiddemo.matching;

import android.graphics.Bitmap;

import com.dft.onyx.FingerprintTemplate;

public class IdentifyPayload {
    private FingerprintTemplate[] referenceTemplates;
    private Bitmap probe;

    public IdentifyPayload(FingerprintTemplate[] referenceTemplates, Bitmap probe) {
        this.setReferenceTemplates(referenceTemplates);
        this.setProbe(probe);
    }

    public FingerprintTemplate[] getReferenceTemplates() {
        return referenceTemplates;
    }

    public void setReferenceTemplates(FingerprintTemplate[] referenceTemplates) {
        this.referenceTemplates = referenceTemplates;
    }

    public Bitmap getProbe() {
        return probe;
    }

    public void setProbe(Bitmap probe) {
        this.probe = probe;
    }
}
