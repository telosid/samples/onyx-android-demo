package com.dft.onyxandroiddemo.matching;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.dft.onyx.core;

import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

/** This is a utility class with example of how to "pyramid" an image.  E.g. output an image
 * at different scales so that the different scales can better approach the scale of a touch-based
 * image from a touch-based sensor.
 */
public class PyramidUtil {
    public static Bitmap centerCropBitmap(Bitmap src, int cropWidth, int cropHeight) {
        float srcCenterX = src.getWidth() / 2.0f;
        float srcCenterY = src.getHeight() / 2.0f;

        float dstCenterX = cropWidth / 2.0f;
        float dstCenterY = cropHeight / 2.0f;

        int minWidth = Math.min(src.getWidth(), cropWidth);
        int minHeight = Math.min(src.getHeight(), cropHeight);
        float halfMinWidth = minWidth / 2.0f;
        float halfMinHeight = minHeight / 2.0f;

        int dstLeft = (int) (dstCenterX - halfMinWidth);
        int dstTop = (int) (dstCenterY - halfMinHeight);
        android.graphics.Rect dstRect = new android.graphics.Rect(
                dstLeft,
                dstTop,
                dstLeft + minWidth,
                dstTop + minHeight
        );

        int srcLeft = (int) (srcCenterX - halfMinWidth);
        int srcTop = (int) (srcCenterY - halfMinHeight);
        android.graphics.Rect srcRect = new android.graphics.Rect(
                srcLeft,
                srcTop,
                srcLeft + minWidth,
                srcTop + minHeight
        );

        Bitmap dst = Bitmap.createBitmap(cropWidth, cropHeight, src.getConfig());
        Canvas canvas = new Canvas(dst);
        canvas.drawBitmap(src, srcRect, dstRect, null);

        return dst;
    }

    public static List<Bitmap> createImagePyramid(Bitmap bitmap, double[] scaleFactors) {
        List<Bitmap> bitmapPyramid = new ArrayList<>();

        // Add original bitmap to pyramid
        bitmapPyramid.add(bitmap);

        // Create scaled bitmaps and add to pyramid
        for (double scaleFactor : scaleFactors) {
            int scaledWidth = (int) (bitmap.getWidth() * scaleFactor);
            int scaledHeight = (int) (bitmap.getHeight() * scaleFactor);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(
                    bitmap,
                    scaledWidth,
                    scaledHeight,
                    true
            );
            bitmapPyramid.add(
                    centerCropBitmap(scaledBitmap, bitmap.getWidth(), bitmap.getHeight())
            );

        }

        // If you need to convert to list of WSQs you can do this
        List<byte[]> wsqPyramid = new ArrayList<>();
        for (Bitmap scaledBitmap: bitmapPyramid) {
            Mat mat = new Mat();
            org.opencv.android.Utils.bitmapToMat(scaledBitmap, mat);
            byte[] scaledWsq = core.matToWsq(mat);
            wsqPyramid.add(scaledWsq);
        }
        // Example submit to AFIS
        for (byte[] wsqBytes : wsqPyramid) {
            // You can now use this list to submit to AFIS in parallel to find a match
            // if no match, you can expand the levels of pyramiding
            // or make the scale more fine-grained, i.e. make the scale factors 5% instead of 10%
        }

        return bitmapPyramid;
    }
}
