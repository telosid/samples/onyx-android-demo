# onyx-android-demo
Demo app for developers that demonstrates the features of Onyx Android SDK

Getting Started
---------------

If you don't already have Android Studio, you can download it <a href="http://developer.android.com/sdk/index.html" target="_blank">here</a>.

Once Android Studio is installed, please contact us to purchase your ONYX license key <a href="http://www.diamondfortress.com/contact" target="_blank">here</a>. <br />
**Note: Make sure you have updated to the latest Android SDK via the SDK Manager.**

You should receive a license of the form XXXX-XXXX-XXXX-X-X at your provided e-mail address.
<br />
Next, you can clone our sample repository on the command-line using the following commands:

    > cd <YOUR_DEVELOPMENT_ROOT>
    > git clone https://gitlab.com/telosid/samples/onyx-android-demo.git

Alternatively, you can clone the project via Android Studio:
<br/>
Select `File >> New >> Project from Version Control >> Repository URL`, and Git URL of
    ```aidl
    https://gitlab.com/telosid/samples/onyx-android-demo.git
    ```

Place your license key into `app/src/main/res/values/license.xml` shown below:

    ...
    <string name="license_key_value">XXXX-XXXX-XXXX-X-X</string>
    ...

The license.xml file should be excluded from version control.

There are customizable string resources that can be overridden from the onyx-camera library by adding
the strings to the `app/src/main/res/values/strings.xml` file.

String to hold the on-screen text to use when user's fingers are too far or too close
`<string name="fingers_not_in_focus_text">Move fingers until in focus</string>`

String to hold the on-screen text to use when user's thumb is too far or too close.
`<string name="thumb_not_in_focus_text">Move thumb until in focus</string>`

String to hold the on-screen text to use when user should hold fingers steady for capture.
`<string name="capture_fingers_text">Hold fingers steady</string>`

String to hold the on-screen text to use when user should hold thumb steady for capture.
`<string name="capture_thumb_text">Hold thumb steady</string>`


Simply replace the strings with the translation that is desired, or use the Android Studio
Translations Editor: https://developer.android.com/studio/write/translations-editor

The Translations Editor is the preferred method to set these strings as translations for multiple 
languages can be managed more seamlessly, but there are additionally `OnyxConfiguration` settings
that allow to set these.  This was done in order to maintain parity with the iOS library, as we have
to manually add strings in iOS for the time being.

This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Build >> Make Project" in Android Studio.

Now plug in your compatible device, and select Run >> Run 'app'.

# Customizing OnyxConfiguration
The Onyx SDK is very highly configurable, and this section will briefly provide an overview of typical configuration.
The typical configuration for OnyxConfiguration will be set to return a Processed image, or a WSQ image,
depending on the needs of the biometric system being utilized.  If doing on device matching, then a
proprietary FingerprintTemplate and matching algorithm from Innovatrics is provided.  The OnyxConfiguration
can be configured to return the FingerprintTemplate, and the example code for doing on-device matching.

If integrating with a 3rd party matching system, such as a national database, or from proprietary vendors,
then the requirements will vary, but will most often require a WSQ image, as these are compressed for sending
across networks.  The size of the output WSQ image can be controlled using the cropSize() method on 
OnyxConfigurationBuilder.  This will set the dimensions (width and height) of the image, such as to 512x512 or 500x500. 
**It is important to set this configuration to what the 3rd party matching system requires for image dimensions

Another important matter is the scale of an image.  This can be controlled using the cropFactor() method
on the OnyxConfigurationBuilder, and will scale the image up or down.  This can be important on some devices
that may focus more closely or more further away than average device camera modules.  We have set the scale
to 1.0f by default that works for most devices, but you may try adjusting this setting for devices that may
focus more closely or more further away to get the scale to more closely match touch-based fingerprint scanners.

The default settings for the options on the OnyxConfigurationBuilder should work fine out of the box.
Here are a couple other settings that may be of use, however.
**useOnyxLive() - this will perform a "liveness" check that returns a decimal that indicates the percentage
certainty that the image is a real finger, and not a "fake" finger/fingerprint or image of a finger/fingerprint.

**useManualCapture() - this will allow the user to touch the screen to initiate the capture process instead
of using the auto-capture process

The Javadoc code can be used in most modern IDE's, but a copy of the OnyxConfigurationBuilder is listed at
the bottom of this document for more advanced usage.

# Integration in to existing projects
If you are integrating in to an existing project, you will want to make sure you have the below copied from this project:
- **Copy the proguard.pro line items in to your project to allow release builds to work correctly!**

Documentation on the OnyxConfigurationBuilder is in the Java code comments, and listed here
for reference:
```

/**
 * Create this class first to build an instance of OnyxConfiguration.  This builder allows
 * you to specify all of the different options that you would like for Onyx.
 */

@Keep
public class OnyxConfigurationBuilder {
    private OnyxConfiguration onyxConfig = new OnyxConfiguration();

    /**
     * Specify the configActivity for configuring ONYX as it can potentially be different
     * from the activityForRunningConfiguredOnyx that is used to run the configured instance of OnyxFragment.
     */
    public OnyxConfigurationBuilder configActivity(Activity configActivity) {
        onyxConfig.setConfigActivity(configActivity);
        return this;
    }

    /**
     * Specify the ONYX license key.
     * @param licenseKey (required) the Onyx license key
     */
    public OnyxConfigurationBuilder licenseKey(String licenseKey) {
        onyxConfig.setLicenseKey(licenseKey);
        return this;
    }

    /**
     * Specify the SuccessCallback that is used to return the OnyxResult.
     * @param successCallback (required) the event handler for the SuccessCallback.
     * @see OnyxConfiguration.SuccessCallback
     */
    public OnyxConfigurationBuilder successCallback(
            OnyxConfiguration.SuccessCallback successCallback) {
        onyxConfig.setSuccessCallback(successCallback);
        return this;
    }

    /**
     * Specify the ErrorCallback that is used to return any errors.
     * @param errorCallback (required) the event handler for the ErrorCallback.
     * @see OnyxConfiguration.ErrorCallback
     */
    public OnyxConfigurationBuilder errorCallback(OnyxConfiguration.ErrorCallback errorCallback) {
        onyxConfig.setErrorCallback(errorCallback);
        return this;
    }

    /**
     * Specify the OnyxCallback that is used to return the configured instance of ONYX.
     * The callback returns the Onyx object used to create Onyx.
     * @param onyxCallback
     */
    public OnyxConfigurationBuilder onyxCallback(
            OnyxConfiguration.OnyxCallback onyxCallback) {
        onyxConfig.setOnyxCallback(onyxCallback);
        return this;
    }

    /**
     * Specify whether or not to return the raw image.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnRawImage(boolean returnRawImage) {
        onyxConfig.setReturnRawImage(returnRawImage);
        return this;
    }

    /**
     * Specify whether or not to return the processed image.  This would be the most
     * common image type to use for conversion to WSQ or for matching.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnProcessedImage(boolean returnProcessedImage) {
        onyxConfig.setReturnProcessedImage(returnProcessedImage);
        return this;
    }

    /**
     * Specify whether or not to return the enhanced image.  This is a special image that
     * works better with certain matching algorithms, but needs special testing to make sure it
     * will match.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnEnhancedImage(boolean returnEnhancedImage) {
        onyxConfig.setReturnEnhancedImage(returnEnhancedImage);
        return this;
    }

    /**
     * Specify whether or not to return a slap image. This contains all fingers captured
     * within a single image as specified in the FBI EBTS format.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnSlapImage(boolean returnSlapImage) {
        onyxConfig.setReturnSlapImage(returnSlapImage);
        return this;
    }

    /**
     * Specify whether or not to return the binarized processed fingerprint image in {@link OnyxResult}
     */
    public OnyxConfigurationBuilder shouldBinarizeProcessedImage(boolean binarizeProcessedImage) {
        onyxConfig.setShouldBinarizeProcessedImage(binarizeProcessedImage);
        return this;
    }

    /**
     * Specify whether to return a FullFrame image.  This returns
     * an image that is suitable for finger detection in a full frame.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnFullFrameImage(boolean returnFullFrameImage) {
        onyxConfig.setReturnFullFrameImage(returnFullFrameImage);
        return this;
    }

    /**
     * Specify the maximum height for the FullFrame image to be returned, so for example,
     * if you want a 1920 height image returned, pass in 1080.0f for the value.  It will a full frame
     * image resized to 1920 for the maximum height.  To get the original height of the full frame, to
     * get full resolution, pass in a value of 1.0f.
     */
    public OnyxConfigurationBuilder fullFrameMaxImageHeight(float fullFrameMaxImageHeight) {
        onyxConfig.setFullFrameMaxImageHeight(fullFrameMaxImageHeight);
        return this;
    }

    /**
     * Specify whether or not the capture task will return the WSQ image.
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnWSQ(boolean returnWSQ) {
        onyxConfig.setReturnWSQ(returnWSQ);
        return this;
    }

    /**
     * This method sets whether or not the capture task will return the specified
     * {@link com.dft.onyxcamera.config.OnyxConfiguration.FingerprintTemplateType} in the
     * {@link OnyxResult}
     * The ISO template is the ISO/IEC 19794-4 standard minutiae based template
     * The INNOVATRICS template is a proprietary fingerprint template that is highly scale tolerant and
     * useful for touchless matching
     * Returned in the {@link OnyxResult}
     */
    public OnyxConfigurationBuilder returnFingerprintTemplate(OnyxConfiguration.FingerprintTemplateType
                                                                         fingerprintTemplateType) {
        onyxConfig.setReturnFingerprintTemplate(fingerprintTemplateType);
        return this;
    }

    /**
     * Specify the crop size for the capture image.  This will determine the image dimensions
     * in length and width for the resulting images.  This should be set to whatever is required
     * for the matching algorithm that is going to be used.
     *
     * @param width sets the width of the center crop area.
     * @param height sets the height of the center crop area.
     */
    public OnyxConfigurationBuilder cropSize(double width, double height) {
        onyxConfig.setCropSize(new Size(width, height));
        return this;
    }

    /**
     * Specify the crop factor for the capture image.  This has a built in scaling factor and
     * this crop factor should be adjusted until you get an image that looks close to images that are
     * known to match against whatever biometric system you are attempting to match against.
     *
     * @param cropFactor
     * @return
     */
    public OnyxConfigurationBuilder cropFactor(double cropFactor) {
        onyxConfig.setCropFactor(cropFactor);
        return this;
    }

    /**
     * Specify that the spinner should be shown during OnyxConfiguration and during
     * image processing.
     *
     * @param showLoadingSpinner
     */
    public OnyxConfigurationBuilder showLoadingSpinner(boolean showLoadingSpinner) {
        onyxConfig.setShowLoadingSpinner(showLoadingSpinner);
        return this;
    }

    /**
     * Specify whether or not to use the manual capture instead of the auto-capture.
     * Manual capture is where the operator will tap the screen in order to capture.
     */
    public OnyxConfigurationBuilder useManualCapture(boolean useManualCapture) {
        onyxConfig.setUseManualCapture(useManualCapture);
        return this;
    }

    /**
     * Specify to use ONYX LIVE_FINGER liveness detection as part of the configuration.
     * This will take additional processing time.
     *
     * @param useOnyxLive
     */
    public OnyxConfigurationBuilder useOnyxLive(boolean useOnyxLive) {
        onyxConfig.setUseOnyxLive(useOnyxLive);
        return this;
    }

    /**
     * Specify to use the flash
     * @param useFlash
     */
    public OnyxConfigurationBuilder useFlash(boolean useFlash) {
        onyxConfig.setUseFlash(useFlash);
        return this;
    }

    /**
     * Specify the orientation of the reticle {@link Reticle.Orientation}
     * @param reticleOrientation
     */
    public OnyxConfigurationBuilder reticleOrientation(Reticle.Orientation reticleOrientation) {
        onyxConfig.setReticleOrientation(reticleOrientation);
        return this;
    }

    /**
     * Specify to compute NFIQ metrics
     */
    public OnyxConfigurationBuilder computeNfiqMetrics(boolean computeNfiqMetrics) {
        onyxConfig.setComputeNfiqMetrics(computeNfiqMetrics);
        return this;
    }

    /**
     * Specify target pixels per inch
     */
    public OnyxConfigurationBuilder targetPixelsPerInch(double pixelsPerInch) {
        onyxConfig.setTargetPixelsPerInch(pixelsPerInch);
        return this;
    }

    /**
     * Specify the subject id
     */
    public OnyxConfigurationBuilder subjectId(String subjectId) {
        onyxConfig.setSubjectId(subjectId);
        return this;
    }

    /**
     * Specify whether to upload capture metrics result
     */
    public OnyxConfigurationBuilder uploadMetrics(boolean uploadMetrics) {
        onyxConfig.setUploadMetrics(uploadMetrics);
        return this;
    }

    /**
     * This method configures Onyx to use a lens focus distance for camera2 API compatible devices.
     * This is used to set a "fixed focus" distance.  The value can vary based on the device
     * capabilities.  If the supplied parameter is not valid for the device, it will attempt a
     * default value based on information reported by the device.  If this does not work, it will
     * fall back to auto-focus for the device.
     */
    public OnyxConfigurationBuilder lensFocusDistanceCamera2(float lensFocusDistanceCamera2) {
        onyxConfig.setLensFocusDistanceCamera2(lensFocusDistanceCamera2);
        return this;
    }

    /**
     * Specify to return OnyxError on low quality imagery
     */
    public OnyxConfigurationBuilder returnOnyxErrorOnLowQuality(boolean returnOnyxErrorOnLowQuality) {
        onyxConfig.setReturnOnyxErrorOnLowQuality(returnOnyxErrorOnLowQuality);
        return this;
    }

    /**
     * Specify Onyx to enable or disable the camera shutter sound if this option
     * is available on the device.  Not all device manufactuers implement this, so it is not
     * guaranteed to work on all devices.  The default is true for this configuration.
     */
    public OnyxConfigurationBuilder enableShutterSound(boolean enableShutterSound) {
        onyxConfig.setEnableShutterSound(enableShutterSound);
        return this;
    }

    /**
     * Notice: This setting only works on devices that support FULL camera2 API support and
     * possibly that have high enough hardware specs to run smoothly.  It may not work on all devices.
     *
     * Specify Onyx to use the Camera2 API for preview image streaming instead
     * of taking still picture image captures, and it takes a lower and upper FPS settings to
     * control the frame rate.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OnyxConfigurationBuilder useCamera2PreviewStreaming(boolean useCamera2PreviewStreaming,
                                                               int lowerFps, int upperFps) {
        onyxConfig.setUseCamera2PreviewStreaming(useCamera2PreviewStreaming);
        Range<Integer>[] fps = new Range[1];
        fps[0] = Range.create(lowerFps,upperFps);
        onyxConfig.setCamera2PreviewStreamingFpsRange(fps);
        return this;
    }

    /**
     * Specify a double value between 0.0 - 1.0 to set the threshold for QualityNet to capture.  Higher
     * values mean higher image quality.
     */
    public OnyxConfigurationBuilder captureQualityThreshold(double captureQualityThreshold) {
        onyxConfig.setCaptureQualityThreshold(captureQualityThreshold);
        return this;
    }

    /**
     * This method builds the OnyxConfiguration object with the specified parameters, and
     * checks that all configuration setup is complete before returning the {@link Onyx} via
     * {@link com.dft.onyxcamera.config.OnyxConfiguration.OnyxCallback#onConfigured onConfigured}
     */
    public void buildOnyxConfiguration() {
        onyxConfig.setCaptureAnimationCallback(new CaptureAnimationCallbackUtil()
                .createCaptureAnimationCallback(onyxConfig.getConfigActivity()));
        Onyx onyx = new Onyx();
        onyx.doSetup(onyxConfig);
    }
}


```

Support
-------

- Diamond Fortress Technologies Support Site: <a href="http://support.diamondfortress.com" target="_blank">support.diamondfortress.com</a>
